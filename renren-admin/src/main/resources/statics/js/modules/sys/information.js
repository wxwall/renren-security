$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'sys/information/list',
        datatype: "json",
        colModel: [			
			{ label: 'id', name: 'id', index: 'id', width: 50, key: true },
            { label: '姓名', name: 'name', index: 'name', width: 80 },
            { label: '性别', name: 'sex', width: 60, formatter: function(value, options, row){
                return value === "1" ?
                    '<span class="label label-success">女</span>' :
                    '<span class="label label-success">男</span>';
            }},
			{ label: '年龄', name: 'age', index: 'age', width: 80 },
			{ label: '职业类型', name: 'workTypeEntities', index: 'workTypeEntities', width: 80,formatter:function (value, options, row) {
			    if(value[0] != null){
			        var result = "";
			        for(var i = 0;i < value.length; i++ ){
			            result = result + value[i].name + ",";
                    }
                    return result;
                }else{
			        return "";
                }
            } },
			{ label: '手机号码', name: 'telphone', index: 'telphone', width: 80 },
			{ label: '住址', name: 'address', index: 'address', width: 80 }, 			
			{ label: '身份证号码', name: 'idcard', index: 'idcard', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
        worktypeList:{},
		information: {
		    worktypeList:[]
        }
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
            vm.worktypeList = {};
			vm.information = {
			    sex:"0",
                worktypeList:[]
            };
			this.getWorkTypeList();
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";


            vm.getInfo(id);
            this.getWorkTypeList();
		},
		saveOrUpdate: function (event) {
		    $('#btnSaveOrUpdate').button('loading').delay(1000).queue(function() {
                var url = vm.information.id == null ? "sys/information/save" : "sys/information/update";
                $.ajax({
                    type: "POST",
                    url: baseURL + url,
                    contentType: "application/json",
                    data: JSON.stringify(vm.information),
                    success: function(r){
                        if(r.code === 0){
                             layer.msg("操作成功", {icon: 1});
                             vm.reload();
                             $('#btnSaveOrUpdate').button('reset');
                             $('#btnSaveOrUpdate').dequeue();
                        }else{
                            layer.alert(r.msg);
                            $('#btnSaveOrUpdate').button('reset');
                            $('#btnSaveOrUpdate').dequeue();
                        }
                    }
                });
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			var lock = false;
            layer.confirm('确定要删除选中的记录？', {
                btn: ['确定','取消'] //按钮
            }, function(){
               if(!lock) {
                    lock = true;
		            $.ajax({
                        type: "POST",
                        url: baseURL + "sys/information/delete",
                        contentType: "application/json",
                        data: JSON.stringify(ids),
                        success: function(r){
                            if(r.code == 0){
                                layer.msg("操作成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            }else{
                                layer.alert(r.msg);
                            }
                        }
				    });
			    }
             }, function(){
             });
		},
		getInfo: function(id){
			$.get(baseURL + "sys/information/info/"+id, function(r){
                vm.information = r.information;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		},
        getWorkTypeList: function(){
            $.get(baseURL + "sys/worktype/select", function(r){
                vm.worktypeList = r.list;
            });
        }
	}
});