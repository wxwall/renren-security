package io.renren.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2020-01-01 15:37:10
 */
@Data
@TableName("work_type")
public class WorkTypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 工种类型
	 */
	@TableId
	private Long id;
	/**
	 * 
	 */
	private String name;
	/**
	 * 工种类型
	 */
	private Integer type;

}
