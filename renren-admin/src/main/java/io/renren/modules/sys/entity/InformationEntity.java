package io.renren.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.UpdateGroup;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * 
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2019-12-29 23:40:01
 */
@Data
@TableName("information")
public class InformationEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 姓名
	 */
	@NotBlank(message="用户名不能为空", groups = {AddGroup.class, UpdateGroup.class})
	private String name;
	/**
	 * 性别
	 */
	@NotBlank(message="性别不能为空", groups = {AddGroup.class, UpdateGroup.class})
	private String sex;
	/**
	 * 年龄
	 */
	@DecimalMin(value = "18",message = "年龄必须在18至60岁之间",groups = {AddGroup.class, UpdateGroup.class})
	@DecimalMax(value = "60",message = "年龄必须在18至60岁之间",groups = {AddGroup.class, UpdateGroup.class})
	private Integer age;

	/**
	 * 手机号码
	 */

	@Length(max = 11, min = 11, message = "手机号的长度必须是11位.",groups = {AddGroup.class, UpdateGroup.class})
	private String telphone;
	/**
	 * 住址
	 */
	private String address;
	/**
	 * 身份证号码
	 */
	@Length(max = 18, min = 18, message = "身份证长度必须是18位.",groups = {AddGroup.class, UpdateGroup.class})
	private String idcard;

	/**
	 * 工种职业ID列表
	 */
	@TableField(exist=false)
	private List<Long> worktypeList;

	@TableField(exist=false)
	List<WorkTypeEntity> workTypeEntities;

}
