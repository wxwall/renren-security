package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.entity.InformationWorkTypeEntity;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2020-01-01 16:23:41
 */
public interface InformationWorkTypeService extends IService<InformationWorkTypeEntity> {


    void saveOrUpdate(Long informationId, List<Long> workTypeIdList);

    PageUtils queryPage(Map<String, Object> params);
}

