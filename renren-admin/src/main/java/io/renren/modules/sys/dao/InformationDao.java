package io.renren.modules.sys.dao;

import io.renren.modules.sys.entity.InformationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2019-12-29 23:40:01
 */
@Mapper
public interface InformationDao extends BaseMapper<InformationEntity> {
	
}
