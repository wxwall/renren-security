package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.sys.dao.WorkTypeDao;
import io.renren.modules.sys.entity.WorkTypeEntity;
import io.renren.modules.sys.service.WorkTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("workTypeService")
public class WorkTypeServiceImpl extends ServiceImpl<WorkTypeDao, WorkTypeEntity> implements WorkTypeService {

    @Autowired
    WorkTypeDao workTypeDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<WorkTypeEntity> page = this.page(
                new Query<WorkTypeEntity>().getPage(params),
                new QueryWrapper<WorkTypeEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<WorkTypeEntity> queryWorkTypeByInformationId(Long id) {
        return workTypeDao.queryWorkTypeByInformationId(id);
    }

}
