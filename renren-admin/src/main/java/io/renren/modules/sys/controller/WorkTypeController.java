package io.renren.modules.sys.controller;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.modules.sys.entity.WorkTypeEntity;
import io.renren.modules.sys.service.WorkTypeService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;



/**
 * 
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2020-01-01 15:37:10
 */
@RestController
@RequestMapping("sys/worktype")
public class WorkTypeController {
    @Autowired
    private WorkTypeService workTypeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:worktype:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = workTypeService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 工种列表
     */
    @RequestMapping("/select")
    public R select(){
        List<WorkTypeEntity> list = workTypeService.list();

        return R.ok().put("list", list);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:worktype:info")
    public R info(@PathVariable("id") Integer id){
        WorkTypeEntity workType = workTypeService.getById(id);

        return R.ok().put("workType", workType);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:worktype:save")
    public R save(@RequestBody WorkTypeEntity workType){
        workTypeService.save(workType);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:worktype:update")
    public R update(@RequestBody WorkTypeEntity workType){
        ValidatorUtils.validateEntity(workType);
        workTypeService.updateById(workType);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:worktype:delete")
    public R delete(@RequestBody Integer[] ids){
        workTypeService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
