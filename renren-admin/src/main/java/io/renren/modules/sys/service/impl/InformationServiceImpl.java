package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.sys.dao.InformationDao;
import io.renren.modules.sys.entity.InformationEntity;
import io.renren.modules.sys.entity.WorkTypeEntity;
import io.renren.modules.sys.service.InformationService;
import io.renren.modules.sys.service.InformationWorkTypeService;
import io.renren.modules.sys.service.WorkTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("informationService")
public class InformationServiceImpl extends ServiceImpl<InformationDao, InformationEntity> implements InformationService {

    @Autowired
    InformationWorkTypeService informationWorkTypeService;

    @Autowired
    WorkTypeService workTypeService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<InformationEntity> page = this.page(
                new Query<InformationEntity>().getPage(params),
                new QueryWrapper<InformationEntity>()
        );

        List<InformationEntity> records = page.getRecords();
        for (InformationEntity record : records) {
            Long id = record.getId();
            List<WorkTypeEntity> workTypeEntities = workTypeService.queryWorkTypeByInformationId(id);
            record.setWorkTypeEntities(workTypeEntities);
        }

        return new PageUtils(page);
    }


    @Override
    public boolean save(InformationEntity entity) {
        boolean b = super.save(entity);
        informationWorkTypeService.saveOrUpdate(entity.getId(),entity.getWorktypeList());
        return b;
    }


    @Override
    public boolean updateById(InformationEntity entity) {
        boolean b = super.updateById(entity);
        informationWorkTypeService.saveOrUpdate(entity.getId(),entity.getWorktypeList());
        return b;
    }
}
