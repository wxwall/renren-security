package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.entity.InformationEntity;

import java.util.Map;

/**
 * 
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2019-12-29 23:40:01
 */
public interface InformationService extends IService<InformationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

