package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.entity.WorkTypeEntity;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2020-01-01 15:37:10
 */
public interface WorkTypeService extends IService<WorkTypeEntity> {

    PageUtils queryPage(Map<String, Object> params);


    List<WorkTypeEntity> queryWorkTypeByInformationId(Long id);
}

