package io.renren.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.sys.dao.InformationWorkTypeDao;
import io.renren.modules.sys.entity.InformationWorkTypeEntity;
import io.renren.modules.sys.service.InformationWorkTypeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("informationWorkTypeService")
public class InformationWorkTypeServiceImpl extends ServiceImpl<InformationWorkTypeDao, InformationWorkTypeEntity> implements InformationWorkTypeService {

    @Override
    public void saveOrUpdate(Long informationId, List<Long> workTypeIdList) {
        //先删除关系
        this.remove(new QueryWrapper<InformationWorkTypeEntity>().eq("informationId", informationId));

        if(workTypeIdList == null || workTypeIdList.size() == 0){
            return ;
        }

        //保存关系
        for(Long workTypeId : workTypeIdList){
            InformationWorkTypeEntity informationWorkTypeEntity = new InformationWorkTypeEntity();
            informationWorkTypeEntity.setInformationid(informationId);
            informationWorkTypeEntity.setWorktypeid(workTypeId);
            this.save(informationWorkTypeEntity);
        }
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<InformationWorkTypeEntity> page = this.page(
                new Query<InformationWorkTypeEntity>().getPage(params),
                new QueryWrapper<InformationWorkTypeEntity>()
        );

        return new PageUtils(page);
    }

}
