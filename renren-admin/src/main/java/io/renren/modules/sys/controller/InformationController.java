package io.renren.modules.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.sys.entity.InformationEntity;
import io.renren.modules.sys.entity.InformationWorkTypeEntity;
import io.renren.modules.sys.service.InformationService;
import io.renren.modules.sys.service.InformationWorkTypeService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * 
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2019-12-29 23:40:01
 */
@RestController
@RequestMapping("sys/information")
public class InformationController {
    @Autowired
    private InformationService informationService;

    @Autowired
    private InformationWorkTypeService informationWorkTypeService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:information:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = informationService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:information:info")
    public R info(@PathVariable("id") Integer id){
        InformationEntity information = informationService.getById(id);
        InformationWorkTypeEntity entityWrapper = new InformationWorkTypeEntity();
        entityWrapper.setInformationid(information.getId());
        QueryWrapper<InformationWorkTypeEntity> informationWorkTypeEntityQueryWrapper = new QueryWrapper<>();
        informationWorkTypeEntityQueryWrapper.setEntity(entityWrapper);
        List<InformationWorkTypeEntity> list = informationWorkTypeService.list(informationWorkTypeEntityQueryWrapper);
        List<Long> idList = list.stream().map(informationWorkTypeResult -> informationWorkTypeResult.getWorktypeid()).collect(Collectors.toList());
        information.setWorktypeList(idList);
        return R.ok().put("information", information);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:information:save")
    public R save(@RequestBody InformationEntity information){

        ValidatorUtils.validateEntity(information, AddGroup.class);
        informationService.save(information);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:information:update")
    public R update(@RequestBody InformationEntity information){
        ValidatorUtils.validateEntity(information, UpdateGroup.class);
        informationService.updateById(information);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:information:delete")
    public R delete(@RequestBody Integer[] ids){
        informationService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
